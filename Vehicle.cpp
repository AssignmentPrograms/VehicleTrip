/*
 * File Name: Vehicle.cpp
 * Author: Madhav Gautam
 * Student ID: P428j884
 * Assignment Number: #2
 */

#include "Vehicle.h"

/* Constructors */
Vehicle::Vehicle()
{
	manufacturer = "OTHER";
	model = "OTHER";
	engine = 0;
	cylinder = 0;
	tankSize = 0;
	cityMPG = 0;
	hwyMPG = 0;
}


Vehicle::Vehicle(std::string manufacturer1,
				 std::string model1,
				 double engine1,
				 int cylinder1,
				 float tankSize1,
				 int cityMPG1,
				 int hwyMPG1)
{
	manufacturer = manufacturer1;
	model = model1;
	cylinder = cylinder1;
	engine = engine1;
	tankSize = tankSize1;
	cityMPG = cityMPG1;
	hwyMPG = hwyMPG1;
}



/* Accessor functions */
std::string Vehicle::getManufacturer() const
{
	return manufacturer;
}

std::string Vehicle::getModel() const
{
	return model;
}

double Vehicle::getEngine() const
{
	return engine;
}

int Vehicle::getCylinder() const
{
	return cylinder;
}

float Vehicle::getTanksize() const
{
	return tankSize;
}

int Vehicle::getCityMPG() const
{
	return cityMPG;
}

int Vehicle::getHwyMPG() const
{
	return hwyMPG;
}

/* Mutator functions */
void Vehicle::setManufacturer(std::string manufacturer1)
{
	manufacturer = manufacturer1;
}

void Vehicle::setModel (std::string model1)
{
	model = model1;
}
/* Mutators */
void Vehicle::setEngine(double engine1) {
	engine = engine1;
}

void Vehicle::setCylinder(int cylinder1)
{
	cylinder = cylinder1;
}

void Vehicle::setTanksize(float tanksize1)
{
	tankSize = tanksize1;
}

void Vehicle::setCityMPG(int cityMPG1)
{
	cityMPG = cityMPG1;
}

void Vehicle::setHwyMPG(int hwyMPG1)
{
	hwyMPG = hwyMPG1;
}
