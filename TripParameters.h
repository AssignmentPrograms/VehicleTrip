/*
 * File Name: TripParameters.h
 * Author: Madhav Gautam
 * Student ID: P428j884
 * Assignment Number: #2
 */

#ifndef TRIPPARAMETERS_H_
#define TRIPPARAMETERS_H_

class TripParameters
{
public:
	/* Default constructor */
	TripParameters();

	/* Read in each value from configuration file (Windows INI file) */
	TripParameters(char fileName[80]);

	void PrintTripParameters();

	/* Accessor functions to retrieve variable data. */
	int getAvgSpeedCity();
	int getAvgSpeedHwy();
	double getAvgFuelPrice();
	double getDistanceGasStation();
	double getTimeNapGap();
	int getRefuelTime();
	int getRestRoomTime();
	int getNapTime();

private:
	/* Member variables that store information regarding the trip*/
		int avgSpeedCity;
		int avgSpeedHwy;
		double avgFuelPrice;
		double distanceGasStation;
		double timeNapGap;
		int refuelTime;
		int restRoomTime;
		int napTime;
};

#endif /* TRIPPARAMETERS_H_ */
