/*
 * File Name: VehicleRecords.h
 * Author: Madhav Gautam
 * Student ID: P428j884
 * Assignment Number: #2
 */

#ifndef VEHICLERECORDS_H
#define VEHICLERECORDS_H

#include <string>
#include <cstring>
#include <fstream>
#include <vector>
#include "Vehicle.h"
#include "VehicleTrip.h"

using namespace std;


class VehicleRecords
{
public:
		VehicleRecords();
		/* Initialize vechile specific variables, read in the values from file*/
        VehicleRecords(char fileName[80]);

        /* Print out Vehicle Records to console. This is for debuging purposes only*/
        void PrintVehicleRecords();

        /* Return the private variable "numberOfVehicles", representing how many vehicles are in member
                 *vector "vehicles"*/
        int getNumberOfVehicles();

        /* Return the member vector vehicles*/
        vector<Vehicle*> getVehicleVector();

private:
        /* Member variables for Vehicle Records, stores data regarding a single Vehicle*/
        int numberOfVehicles;
        vector<Vehicle*> vehicles;
        std::string manufacturer;
		std::string model;
		double engine ;
		int cylinder;
		float tankSize;
		int cityMPG;
		int hwyMPG;
};

#endif /* VEHICLERECORDS_H */
