/*
 * File Name: TripLeg.cpp
 * Author: Madhav Gautam
 * Student ID: P428j884
 * Assignment Number: #2
 */

#include "TripLeg.h"

/* Using default constructors set default value */
TripLeg :: TripLeg()
{
	distance = 0;
	type = CITY;
}

/* Using constructor to set data members */
TripLeg::TripLeg(double distance1, Type type1)
{
	distance = distance1;
	type = type1;
}

/* Accessor functions */
double TripLeg::getDistance()
{
	return distance;
}

TripLeg::Type TripLeg::getType()
{
	return type;
}

/* Mutator functions */
void TripLeg::setDistance(double distance1)
{
	distance = distance1;
}

void TripLeg::setType(Type type1)
{
	type = type1;
}

/* Functions used to check City or Highway */
bool TripLeg::isCity()
{
	if(type == CITY)
		return 1;
	else
		return 0;
}

bool TripLeg::isHighway()
{
	if(type == HIGHWAY)
		return 1;
	else
		return 0;
}


