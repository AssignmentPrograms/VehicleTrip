/*
 * File Name: TripParameters.cpp
 * Author: Madhav Gautam
 * Student ID: P428j884
 * Assignment Number: #2
 */

#include "TripParameters.h"
#include <fstream>
#include <cstdlib>
#include <iostream>
#include <string>

using namespace std;

#define AVGSPEEDCITDEFAULT 25
#define AVGSPEEDHWYDEFAULT 70
#define AVGFUELPRICEDEFAULT 2.19
#define DISTANCEGASSTATIONDEFAULT 80.0
#define TIMENAPGAPDEFAULT 8.0
#define REFUELTIMEDEFAULT 20
#define RESTROOMTIMEDEFAULT 10
#define NAPTIMEDEFAULT 15

/* Default constructor */
TripParameters::TripParameters()
{
	avgSpeedCity = AVGSPEEDCITDEFAULT;
	avgSpeedHwy = AVGSPEEDHWYDEFAULT;
	avgFuelPrice = AVGFUELPRICEDEFAULT;
	distanceGasStation = DISTANCEGASSTATIONDEFAULT;
	timeNapGap = TIMENAPGAPDEFAULT;
	refuelTime = REFUELTIMEDEFAULT;
	restRoomTime = RESTROOMTIMEDEFAULT;
	napTime = NAPTIMEDEFAULT;
}

/* Read in each value from configuration file (Windows INI file) */
TripParameters::TripParameters(char fileName[80])
{
	try {
		char next;
		ifstream instream(fileName);
		/* checks if a file open's successfully and send throw for exception*/
		if(instream.fail()) {
			throw 'f';
		}

		/* Read in values after each '=', modified for efficiency reasons*/
		instream.ignore(1000, '=');

		/* Value counter is to keep track of what data we are reading in*/
		int value_counter = 1;
		while(instream >> next) {
			string tempText;
			instream.ignore(1000, '=');
			getline(instream,tempText);
//			cout << "TempText : " << tempText << endl;
//			std::cin.get();
			if( tempText.find_first_not_of("1234567890.-\n\r ") != string::npos ) {
				cout << "invalid number: " << tempText << endl;
				throw tempText;
			}

			switch(value_counter) {
			case 1:
					avgSpeedCity = atoi(tempText.c_str());
					break;
			case 2:
					avgSpeedHwy = atoi(tempText.c_str());
					break;
			case 3:
					avgFuelPrice = atof(tempText.c_str());
					break;
			case 4:
					distanceGasStation = atof(tempText.c_str());
					break;
			case 5:
					timeNapGap = atof(tempText.c_str());
					break;
			case 6:
					refuelTime = atoi(tempText.c_str());
					break;
			case 7:
					restRoomTime = atoi(tempText.c_str());
					break;
			case 8:
					napTime = atoi(tempText.c_str());
					break;
			}
			value_counter++;
		}
		instream.close();
	}

	/* exception catch if file is failed to open */
	catch (char e) {
		cerr << "\"" << fileName << "\"" << " file failed to open." << endl;
		cerr << "Please check if this file \"" << fileName << "\" is present in project location.\n" << endl;
		cerr << "Program will exit now!!!!";
		std::cin.get();
		exit(1);
	}

	/* exception catch if non digit included in input parameter value */
	catch (string e) {
		cerr << "Error while converting string \"" << e << "\" in int or double in \"" << fileName << "\" File." << endl;
		cerr << "please correct string \"" << e << "\" with proper digit so that it can convert \n" << endl;
		cerr << "Program will exit now!!!!";
		std::cin.get();
		exit(-1);
	}
}

/* Print the Trip Parameter class variables for debugging purposes.*/
void TripParameters::PrintTripParameters()
{
	cout << "Average Speed City = " << getAvgSpeedCity() << " km/h" << endl;
	cout << "Average Speed Hwy = " << getAvgSpeedHwy() << " km/h" << endl;
	cout << "Average Fuel Price = " << getAvgFuelPrice() << " $/l" << endl;
	cout << "Gas Station Distance = " << getDistanceGasStation() << " km" << endl;
	cout << "NapGap Time = " << getTimeNapGap() << " s" << endl;
	cout << "Refuel Time= " << getRefuelTime() << " s" << endl;
	cout << "Rest Room Time = " << getRestRoomTime() << " s" << endl;
}

/* Accessor functions to retrieve variable data.*/
int TripParameters::getAvgSpeedCity()
{
    return avgSpeedCity;
}
int TripParameters::getAvgSpeedHwy()
{
    return avgSpeedHwy;
}
double TripParameters::getAvgFuelPrice()
{
    return avgFuelPrice;
}
double TripParameters::getDistanceGasStation()
{
    return distanceGasStation;
}
double TripParameters::getTimeNapGap()
{
    return timeNapGap;
}
int TripParameters::getRefuelTime()
{
    return refuelTime;
}
int TripParameters::getRestRoomTime()
{
    return restRoomTime;
}
int TripParameters::getNapTime()
{
    return napTime;
}
