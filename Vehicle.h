/*
 * File Name: Vehicle.h
 * Author: Madhav Gautam
 * Student ID: P428j884
 * Assignment Number: #2
 */

#ifndef VEHICLE_H_
#define VEHICLE_H_

#include <iostream>

class Vehicle
{
public:
	/* Constructors */
	Vehicle();
	Vehicle(std::string  manufacturer1,
			std::string model1,
			double engine1,
			int cylinder1,
			float tankSize1,
			int cityMPG1,
			int hwyMPG1);

	/* Accessor functions */
	std::string getManufacturer() const;
	std::string getModel() const;
	double getEngine() const;
	int getCylinder() const;
	float getTanksize() const;
	int getCityMPG() const;
	int getHwyMPG() const;

	/* Mutator functions */
	void setManufacturer(std::string  manufacturer1);
	void setModel (std::string model1);
	void setEngine(double engine1);
	void setCylinder(int culinder1);
	void setTanksize(float tanksize1);
    void setCityMPG(int cityMPG1);
    void setHwyMPG(int hwyMPG1);

    /* Vehicles data members */
private:
	std::string manufacturer;
	std::string model;
	double engine ;
	int cylinder;
	float tankSize;
	int cityMPG;
	int hwyMPG;
};

#endif /* VEHICLE_H_ */
