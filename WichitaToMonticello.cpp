/*
 * File Name: WichitaToMonticello.cpp
 * Author: Madhav Gautam
 * Student ID: P428j884
 * Assignment Number: #2
 */

#include <iostream>
#include <string>
#include <iomanip>
#include <stdlib.h>
#include <vector>
#include <math.h>
#include "TripLeg.h"
#include "Vehicle.h"
#include "VehicleTrip.h"
#include "TripParameters.h"
#include "VehicleRecords.h"

#define TRIPLEGLENGTH 29

using namespace std;

/*Function prototypes*/
void tripLegArrayInit(TripLeg tripLegArray[]);
void vehicleArrayInit(Vehicle* vehicleArray[]);
void vehicleTripValueInit(vector<Vehicle*> vehicleArray,
		TripLeg tripLegArray[],
		int avgSpeedCity,
		int avgSpeedHwy,
		double avgFuelPrice,
		double distanceGasStation,
		double timeNapGap,
		int refuelTime,
		int restRoomTime,
		int napTime,
		int vehicleNumber);
int printVehicleTripOutput(Vehicle* vehicleData, VehicleTrip vehicleTripData);
void PrintInfoToFile(vector<Vehicle*> vehicles, VehicleTrip tripData[], int numberOfVehicles, int extremes[]);

int main()
{
	/*Initializing the array*/
	TripLeg tripLegArray[TRIPLEGLENGTH];

	/*Initializing variables for user input*/
	char parametersFileName[80] = "WichitaToMonticello-Input.ini";
	char vehicleFileName[80] = "WichitaToMonticello-Vehicles.txt";
	int vehicleNumber;

	TripParameters TripParameters(parametersFileName);

	/* Initialize TripLeg Array with values */
	tripLegArrayInit(tripLegArray);

	// Initialize Vehicle Records with values
	VehicleRecords vehicleRecords(vehicleFileName);

	vector<Vehicle*> vehicleArray = vehicleRecords.getVehicleVector();

	/*Finding number for vehicles array*/
	vehicleNumber = vehicleRecords.getNumberOfVehicles();

	vehicleTripValueInit(vehicleArray,
			tripLegArray,
			TripParameters.getAvgSpeedCity(),
			TripParameters.getAvgSpeedHwy(),
			TripParameters.getAvgFuelPrice(),
			TripParameters.getDistanceGasStation(),
			TripParameters.getTimeNapGap(),
			TripParameters.getRefuelTime(),
			TripParameters.getRestRoomTime(),
			TripParameters.getNapTime(),
			vehicleNumber);

	std::cin.clear();
	std::cin.get();
	return 0;
}
/* Initializing the triplegs using the given table */
void tripLegArrayInit(TripLeg tripLegArray[])
{
	tripLegArray[0] = TripLeg(3.3, TripLeg::CITY);
	tripLegArray[1] = TripLeg(23.2, TripLeg::HIGHWAY);
	tripLegArray[2] = TripLeg(0.05, TripLeg::CITY);
	tripLegArray[3] = TripLeg(0.2, TripLeg::CITY);
	tripLegArray[4] = TripLeg(56.2, TripLeg::HIGHWAY);
	tripLegArray[5] = TripLeg(50.3, TripLeg::HIGHWAY);
	tripLegArray[6] = TripLeg(6.8, TripLeg::HIGHWAY);
	tripLegArray[7] = TripLeg(53.5, TripLeg::HIGHWAY);
	tripLegArray[8] = TripLeg(21.3, TripLeg::CITY);
	tripLegArray[9] = TripLeg(229, TripLeg::HIGHWAY);
	tripLegArray[10] = TripLeg(2.8, TripLeg::CITY);
	tripLegArray[11] = TripLeg(74.7, TripLeg::HIGHWAY);
	tripLegArray[12] = TripLeg(47.3, TripLeg::HIGHWAY);
	tripLegArray[13] = TripLeg(69.3, TripLeg::HIGHWAY);
	tripLegArray[14] = TripLeg(0.2, TripLeg::HIGHWAY);
	tripLegArray[15] = TripLeg(24.3, TripLeg::CITY);
	tripLegArray[16] = TripLeg(21.2, TripLeg::CITY);
	tripLegArray[17] = TripLeg(79.2, TripLeg::HIGHWAY);
	tripLegArray[18] = TripLeg(208, TripLeg::HIGHWAY);
	tripLegArray[19] = TripLeg(181.3, TripLeg::HIGHWAY);
	tripLegArray[20] = TripLeg(86.6, TripLeg::HIGHWAY);
	tripLegArray[21] = TripLeg(106.7, TripLeg::HIGHWAY);
	tripLegArray[22] = TripLeg(8.0, TripLeg::HIGHWAY);
	tripLegArray[23] = TripLeg(45.6, TripLeg::CITY);
	tripLegArray[24] = TripLeg(0.1, TripLeg::CITY);
	tripLegArray[25] = TripLeg(0.5, TripLeg::CITY);
	tripLegArray[26] = TripLeg(22.7, TripLeg::HIGHWAY);
	tripLegArray[27] = TripLeg(0.6, TripLeg::CITY);
	tripLegArray[28] = TripLeg(1.7, TripLeg::CITY);
}

/* Initializing the Vehicle trip values */
void vehicleTripValueInit(vector<Vehicle*> vehicleArray,
		TripLeg tripLegArray[],
		int avgSpeedCity,
		int avgSpeedHwy,
		double avgFuelPrice,
		double distanceGasStation,
		double timeNapGap,
		int refuelTime,
		int restRoomTime,
		int napTime,
		int vehicleNumber)
{
	/* Temporary vehicle comparison values */
	int firstReachVehicleIndex = 0;
	int lastReachVehicleIndex = 0;
	int leastCostFuelAddedVehicleIndex = 0;
	int mostCostFuelAddedVehicleIndex = 0;
	int leastCostFuelUsedVehicleIndex = 0;
	int mostCostFuelUsedVehicleIndex = 0;
	double firstReachVehicleTime = 65555555;
	double lastReachVehicleTime = 0;
	double leastCostFuelAddedVehicleCost = 65555555;
	double mostCostFuelAddedVehicleCost = 0;
	double leastCostFuelUsedVehicleCost = 65555555;
	double mostCostFuelUsedVehicleCost = 0;
	VehicleTrip vehicleTripValue[vehicleNumber];

	/* Setting trip values */
	for (int i = 0; i < vehicleNumber; i++) {
		vehicleTripValue[i] = VehicleTrip( vehicleArray[i],
				tripLegArray,
				TRIPLEGLENGTH,
				avgSpeedCity,
				avgSpeedHwy,
				avgFuelPrice,
				distanceGasStation,
				timeNapGap,
				refuelTime,
				restRoomTime,
				napTime);
		/* condition to find first vehicle index to reach Jefferson's Monticello */
		if (firstReachVehicleTime > vehicleTripValue[i].getTotalTimeinMinute()) {
			firstReachVehicleTime = vehicleTripValue[i].getTotalTimeinMinute();
			firstReachVehicleIndex = i;
		}

		/* condition to find last vehicle index to reach Jefferson's Monticello*/
		if (lastReachVehicleTime < vehicleTripValue[i].getTotalTimeinMinute()) {
			lastReachVehicleTime = vehicleTripValue[i].getTotalTimeinMinute();
			lastReachVehicleIndex = i;
		}

		/* condition to find vehicle index costing the least to to reach Jefferson's Monticello based on fuel added to tank */
		if (leastCostFuelAddedVehicleCost > vehicleTripValue[i].getTotalCostFuelAdded()) {
			leastCostFuelAddedVehicleCost = vehicleTripValue[i].getTotalCostFuelAdded();
			leastCostFuelAddedVehicleIndex = i;
		}

		/* condition to find vehicle index costing the most to to reach Jefferson's Monticello based on fuel added to tank */
		if (mostCostFuelAddedVehicleCost < vehicleTripValue[i].getTotalCostFuelAdded()) {
			mostCostFuelAddedVehicleCost = vehicleTripValue[i].getTotalCostFuelAdded();
			mostCostFuelAddedVehicleIndex = i;
		}

		/* condition to find vehicle index costing the least to to reach Jefferson's Monticello based on fuel used to tank */
		if (leastCostFuelUsedVehicleCost > vehicleTripValue[i].getTotalCostFuelUsed()) {
			leastCostFuelUsedVehicleCost = vehicleTripValue[i].getTotalCostFuelUsed();
			leastCostFuelUsedVehicleIndex = i;
		}

		/* condition to find vehicle index costing the most to to reach Jefferson's Monticello based on fuel used to tank */
		if (mostCostFuelUsedVehicleCost < vehicleTripValue[i].getTotalCostFuelUsed()) {
			mostCostFuelUsedVehicleCost = vehicleTripValue[i].getTotalCostFuelUsed();
			mostCostFuelUsedVehicleIndex = i;
		}
	}

	int extremes[6];
	for(int i = 0; i < 6; i++) {
			switch(i) {
			case 0:
					extremes[i] = firstReachVehicleIndex;
					break;
			case 1:
					extremes[i] = lastReachVehicleIndex;
					break;
			case 2:
					extremes[i] = leastCostFuelAddedVehicleIndex;
					break;
			case 3:
					extremes[i] = mostCostFuelAddedVehicleIndex;
					break;
			case 4:
					extremes[i] = leastCostFuelUsedVehicleIndex;
					break;
			case 5:
					extremes[i] = mostCostFuelUsedVehicleIndex;
					break;
			}
	}

	PrintInfoToFile(vehicleArray, vehicleTripValue, vehicleNumber, extremes);

	/* Outputting total miles travel city and highway */
	std::cout << std::fixed;
	std::cout << std::endl << "Total miles driven = " << std::setprecision(2) << vehicleTripValue[0].getTotalCityDriven()+vehicleTripValue[0].getTotalHwyDriven() << "\t";
	std::cout << "(city = " << std::setprecision(2) << vehicleTripValue[0].getTotalCityDriven();
	std::cout << ", highway = "  << std::setprecision(2) << vehicleTripValue[0].getTotalHwyDriven() << ")\n\n";
	/* Outputting vehicle results after comparison */
	std::cout << "Vehicle arriving first at Jefferson's Monticello:" << std::endl;
	printVehicleTripOutput(vehicleArray[firstReachVehicleIndex], vehicleTripValue[firstReachVehicleIndex]);

	std::cout << "Vehicle arriving last at Jefferson's Monticello:" << std::endl;
	printVehicleTripOutput(vehicleArray[lastReachVehicleIndex], vehicleTripValue[lastReachVehicleIndex]);

	std::cout << "Vehicle costing the least to reach Jefferson's Monticello based on fuel added to tank:" << std::endl;
	printVehicleTripOutput(vehicleArray[leastCostFuelAddedVehicleIndex], vehicleTripValue[leastCostFuelAddedVehicleIndex]);

	std::cout << "Vehicle costing the most to reach Jefferson's Monticello based on fuel added to tank:" << std::endl;
	printVehicleTripOutput(vehicleArray[mostCostFuelAddedVehicleIndex], vehicleTripValue[mostCostFuelAddedVehicleIndex]);

	std::cout << "Vehicle costing the least to reach Jefferson's Monticello based on actual fuel used:" << std::endl;
	printVehicleTripOutput(vehicleArray[leastCostFuelUsedVehicleIndex], vehicleTripValue[leastCostFuelUsedVehicleIndex]);

	std::cout << "Vehicle costing the most to reach Jefferson's Monticello based on actual fuel used:" << std::endl;
	printVehicleTripOutput(vehicleArray[mostCostFuelUsedVehicleIndex], vehicleTripValue[mostCostFuelUsedVehicleIndex]);
}

/* Printing vehicle trip details */
int printVehicleTripOutput(Vehicle* vehicleData, VehicleTrip vehicleTripData )
{
	int time[3];
	std::cout << vehicleData->getManufacturer() << " " ;
	std::cout << vehicleData->getModel() << "\t";
	std::cout << "Tank Size = " << vehicleData->getTanksize() << "\t";
	std::cout << "City MPG = " << vehicleData->getCityMPG() << "\t";
	std::cout << "Highway MPG = " << vehicleData->getHwyMPG() << std::endl;

	std::cout << "Trip time (minutes) = " << round(vehicleTripData.getTotalTimeinMinute()) << "\t\t";
	vehicleTripData.getTimeinStd(time);
	std::cout << "Trip time (d.hh:mm) = " << time[0] <<"." << std::setw(2) << std::setfill('0') << time[1] << ":" << std::setw(2) << std::setfill('0') << time[2] << std::endl;

	std::cout << std::fixed;

	std::cout << "Trip cost based on fuel added = " << std::setprecision(2) << vehicleTripData.getTotalCostFuelAdded() << "\t";
	std::cout << "Trip cost based on fuel used = " << std::setprecision(2) << vehicleTripData.getTotalCostFuelUsed() << std::endl;

	std::cout << "Fuel added = " << std::setprecision(4) << vehicleTripData.getFuelAdded() << "\t" ;
	std::cout << "Fuel used = " << std::setprecision(4) << vehicleTripData.getTotalFuelUsed() << "\t" ;
	std::cout << "Fuel remaining = " << std::setprecision(4) << vehicleTripData.getRestFuel() << "\t" ;
	std::cout << "Fuel stops = "  << vehicleTripData.getFuelAddCntr() << std::endl << std::endl;
	return 0;
}

void PrintInfoToFile(vector<Vehicle*> vehicles, VehicleTrip tripData[], int numberOfVehicles, int extremes[])
{
	char outputFileName[80] = "WichitaToMonticello-Results.txt";
	try {

        ofstream outstream(outputFileName);
        if(outstream.fail()) {
        	throw 'f';
        }

        outstream << "Make|Model|EngineSize|EngineCylinders|TankSize|MpgCity|MpgHighway|Time(minutes)|Time(d.hh:mm)|CostFueldAdded|CostFuelUsed|FuelAdded|FuelUsed|FuelRemaining|FuelStops" << endl;

        int time[3];

        for(int i = 0; i < numberOfVehicles; i++) {
			tripData[i].getTimeinStd(time);
			outstream << vehicles[i]->getManufacturer() << "|";
			outstream << vehicles[i]->getModel() << "|";
			outstream << std::fixed;
			outstream << setprecision(1) << vehicles[i]->getEngine() << "|";
			outstream << vehicles[i]->getCylinder() << "|";
			outstream << setprecision(1) << vehicles[i]->getTanksize() << "|";
			outstream << vehicles[i]->getCityMPG() << "|";
			outstream << vehicles[i]->getHwyMPG() << "|";
			outstream << setprecision(0) << tripData[i].getTotalTimeinMinute() << "|";
			outstream << time[0] <<"." << setw(2) << setfill('0') << time[1] << ":" << setw(2) << setfill('0') << time[2] << "|";
			outstream << setprecision(2) << tripData[i].getTotalCostFuelAdded() << "|";
			outstream << setprecision(2) << tripData[i].getTotalCostFuelUsed() << "|";
			outstream << setprecision(4) << tripData[i].getFuelAdded() << "|";
			outstream << setprecision(4) << tripData[i].getTotalFuelUsed() << "|";
			outstream << setprecision(4) << tripData[i].getRestFuel() << "|";
			outstream << tripData[i].getFuelAddCntr();
			outstream << endl;
        }
		outstream << endl;

		for(int i = 0; i < 6; i++) {
			tripData[extremes[i]].getTimeinStd(time);
			outstream << vehicles[extremes[i]]->getManufacturer() << "|";
			outstream << vehicles[extremes[i]]->getModel() << "|";
			outstream << std::fixed;
			outstream << setprecision(1) << vehicles[extremes[i]]->getEngine() << "|";
			outstream << vehicles[extremes[i]]->getCylinder() << "|";
			outstream << setprecision(1) << vehicles[extremes[i]]->getTanksize() << "|";
			outstream << vehicles[extremes[i]]->getCityMPG() << "|";
			outstream << vehicles[extremes[i]]->getHwyMPG() << "|";
			outstream << setprecision(0) << tripData[extremes[i]].getTotalTimeinMinute() << "|";
			outstream << time[0] <<"." << setw(2) << setfill('0') << time[1] << ":" << setw(2) << setfill('0') << time[2] << "|";
			outstream << setprecision(2) << tripData[extremes[i]].getTotalCostFuelAdded() << "|";
			outstream << setprecision(2) << tripData[extremes[i]].getTotalCostFuelUsed() << "|";
			outstream << setprecision(4) << tripData[extremes[i]].getFuelAdded() << "|";
			outstream << setprecision(4) << tripData[extremes[i]].getTotalFuelUsed() << "|";
			outstream << setprecision(4) << tripData[extremes[i]].getRestFuel() << "|";
			outstream << tripData[extremes[i]].getFuelAddCntr();
			outstream << "  #";
			switch(i) {
			case 0:
					outstream << "Vehicle arriving first";
					break;
			case 1:
					outstream << "Vehicle arriving last";
					break;
			case 2:
					outstream << "Vehicle costing least (fuel added)";
					break;
			case 3:
					outstream << "Vehicle costing most (fuel added)";
					break;
			case 4:
					outstream << "Vehicle costing least (fuel used)";
					break;
			case 5:
					outstream << "Vehicle costing most (fuel used)";
					break;
			}
			outstream << endl;
		}
		outstream.close();
		cout << "File \"" << outputFileName << "\" is successfully created with result." << endl;
	}
	catch (char e) {
		cerr << "File \"" << outputFileName << "\" failed to create." << endl;
	}
}
