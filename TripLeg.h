/*
 * File Name: TripLeg.h
 * Author: Madhav Gautam
 * Student ID: P428j884
 * Assignment Number: #2
 */

#ifndef TRIPLEG_H_
#define TRIPLEG_H_

class TripLeg
{
public:
	enum Type{CITY, HIGHWAY};

	/* Class Constructors */
	TripLeg();
	TripLeg(double distance1,Type type1);

	/* Accessor functions */
	double getDistance();
	Type getType();

	/* Mutator functions */
	void setDistance(double distance1);
	void setType(Type type1);

	/* function */
	bool isCity();
	bool isHighway();

private:
	/* class variable */
	double distance;
	Type type;


};

#endif /* TRIPLEG_H_ */
