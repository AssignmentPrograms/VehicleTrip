/*
 * File Name: VehicleTrip.cpp
 * Author: Madhav Gautam
 * Student ID: P428j884
 * Assignment Number: #2
 */

#include <math.h>
#include "VehicleTrip.h"
#include <iostream>

/* Default Constructor */
VehicleTrip::VehicleTrip()
{
	vehicle = new Vehicle;

	/* Initializing the default value */
	fuelAddCntr = 0;
	totalCostFuelUsed = 0;
	totalCostFuelAdded = 0;
	totalCityDistance = 0;
	totalHwyDistance = 0;
	totalDriveTime = 0;
	totalTimeInMin = 0;
	restFuel = 0;
	fuelAdded = 0;
	totalFuelUsed = 0;
}

/* Copy constructor
 * NOTE: This will be called even by parameters that do not containing
 *'&' resulting in a copy being created as opposed to passing in address
 */
VehicleTrip::VehicleTrip(const VehicleTrip &vehicleTrip)
{
	this->vehicle = new Vehicle(*vehicleTrip.vehicle);

	this->fuelAddCntr = vehicleTrip.fuelAddCntr;
	this->totalCostFuelUsed = vehicleTrip.totalCostFuelUsed;
	this->totalCostFuelAdded = vehicleTrip.totalCostFuelAdded;
	this->totalCityDistance = vehicleTrip.totalCityDistance;
	this->totalHwyDistance = vehicleTrip.totalHwyDistance;
	this->totalDriveTime = vehicleTrip.totalDriveTime;
	this->totalTimeInMin = vehicleTrip.totalTimeInMin;
	this->restFuel = vehicleTrip.restFuel;
	this->fuelAdded = vehicleTrip.fuelAdded;
	this->totalFuelUsed = vehicleTrip.totalFuelUsed;
}

/*Overload for operator= to take care of vehicleTrip values */
VehicleTrip& VehicleTrip::operator=(const VehicleTrip &vehicleTrip)
{
	if(this->vehicle != NULL)
	{
		delete this->vehicle;
	}

	this->vehicle = new Vehicle(*vehicleTrip.vehicle);

	this->fuelAddCntr = vehicleTrip.fuelAddCntr;
	this->totalCostFuelUsed = vehicleTrip.totalCostFuelUsed;
	this->totalCostFuelAdded = vehicleTrip.totalCostFuelAdded;
	this->totalCityDistance = vehicleTrip.totalCityDistance;
	this->totalHwyDistance = vehicleTrip.totalHwyDistance;
	this->totalDriveTime = vehicleTrip.totalDriveTime;
	this->totalTimeInMin = vehicleTrip.totalTimeInMin;
	this->restFuel = vehicleTrip.restFuel;
	this->fuelAdded = vehicleTrip.fuelAdded;
	this->totalFuelUsed = vehicleTrip.totalFuelUsed;

	return *this;
}

/* Setting actual value for vehicle trip class member */
VehicleTrip::VehicleTrip(Vehicle* singleVehicle,
		TripLeg tripLegArray[],
						 int tripLegLength,
						 int avgSpeedCity,
						 int avgSpeed_Hwy,
						 double avgFuelPrice,
						 double distanceGasStation,
						 double timeNapGap,
						 int refuelTime,
						 int restRoomTime,
						 int napTime)
{
	/* Initializing the variables and defining the temporary variables */
	double fuelUsed;
	double fuelAddedAmount = 0;
	double totalTripDistance = 0;
	int napCntr = 0;

	fuelAddCntr = 0;
	totalCostFuelUsed = 0;
	totalCostFuelAdded = 0;
	totalCityDistance = 0;
	totalHwyDistance = 0;
	totalDriveTime = 0;
	totalTimeInMin = 0;
	restFuel = singleVehicle->getTanksize();
	fuelAdded = 0;
	totalFuelUsed = 0;

	for (int i = 0; i < tripLegLength ; i++ ) {
		if (tripLegArray[i].getType() == 0) {
			totalCityDistance += tripLegArray[i].getDistance();
			totalDriveTime += (60 * tripLegArray[i].getDistance()) / avgSpeedCity;
			totalTimeInMin += (60 * tripLegArray[i].getDistance()) / avgSpeedCity;
			fuelUsed = (1 / (double)singleVehicle->getCityMPG()) * tripLegArray[i].getDistance();
//			if ((int)(totalTripDistance + tripLegArray[i].getDistance())/distanceGasStation != fuelAddCntr)
				fuelAddedAmount = isFuelAdded(totalTripDistance,
						tripLegArray[i].getDistance(),
						restFuel,
						singleVehicle->getCityMPG(),
						singleVehicle->getCityMPG(),
						singleVehicle->getTanksize(),
						distanceGasStation);
		} else {
			totalHwyDistance += tripLegArray[i].getDistance();
			totalDriveTime += (60 * tripLegArray[i].getDistance()) / avgSpeed_Hwy;
			totalTimeInMin += (60 * tripLegArray[i].getDistance()) / avgSpeed_Hwy;
			fuelUsed = (1 / (double)singleVehicle->getHwyMPG()) * tripLegArray[i].getDistance();
//			if ((int)(totalTripDistance + tripLegArray[i].getDistance())/distanceGasStation != fuelAddCntr)
				fuelAddedAmount = isFuelAdded(totalTripDistance,
						tripLegArray[i].getDistance(),
						restFuel,
						singleVehicle->getCityMPG(),
						singleVehicle->getHwyMPG(),
						singleVehicle->getTanksize(),
						distanceGasStation);
		}
		totalFuelUsed += fuelUsed;
		restFuel = restFuel - fuelUsed + fuelAddedAmount;
		fuelAdded += fuelAddedAmount;
		totalTripDistance += tripLegArray[i].getDistance();
		if (fuelAddedAmount != 0) {
			fuelAddCntr++;
//			std::cout << "fuel added amount = " << fuelAddedAmount << "trip array distance = " << tripLegArray[i].getDistance() << "\n";
			totalTimeInMin += refuelTime;
			if (fuelAddCntr % 2 == 0)
				totalTimeInMin += restRoomTime;
		}

		if ((int)(totalDriveTime / (timeNapGap * 60)) != napCntr) {
			napCntr++;
			totalTimeInMin += napTime;
		}
	}
	totalCostFuelUsed = totalFuelUsed * avgFuelPrice;
	totalCostFuelAdded = (fuelAdded) * avgFuelPrice;

}

/*
 * this isFuelAdded function checks if in next Fuel station
 * fuel need to be added or not and if yes calculate
 * the amount of fuel added and return it.
 */

double VehicleTrip::isFuelAdded(double currentDistance,
		double nextDistance,
		double lastRestFuel,
		int cityMPG,
		int mpg,
		double tankSize,
		double distanceGasStation)
{
	/* temporary variable */
	double temp = 0;
	double fuelReq = 0;
	double refuelAmount = 0;
	double nextGasStationDistance;

	/* Total next Gas Station Distance from start */
	temp = ceil(currentDistance / distanceGasStation) * distanceGasStation;
	/*  distance to reach next Tank Station */
	nextGasStationDistance = temp - currentDistance;

	if (nextGasStationDistance < nextDistance) {
		temp = nextDistance;
		do {
			/* calculates fuel required to reach gas station */
			fuelReq = (1 / (double)mpg) * nextGasStationDistance;
			/*  calculates remaining gas while reached at gas station */
			lastRestFuel = lastRestFuel - fuelReq;
			/* check if with remaining gas is enough to reach next possible gas station with taking City MPG */
			if (lastRestFuel * mpg > distanceGasStation) {
				temp = temp - nextGasStationDistance;
				if(temp >= distanceGasStation)
					nextGasStationDistance = distanceGasStation;
				else
					nextGasStationDistance = temp;
			} else {
				refuelAmount = tankSize - lastRestFuel;
			}
		} while (lastRestFuel * mpg > distanceGasStation && nextGasStationDistance == distanceGasStation);
	}
	return refuelAmount;
}

VehicleTrip::~VehicleTrip()
{
	if(!this->vehicle)
	{
		delete this->vehicle;
	}
}

/* Accessor functions */
double VehicleTrip::getTotalCostFuelUsed()
{
	return totalCostFuelUsed;
}

double VehicleTrip::getTotalCostFuelAdded()
{
	return totalCostFuelAdded;
}

double VehicleTrip::getTotalCityDriven()
{
	return totalCityDistance;
}

double VehicleTrip::getTotalHwyDriven()
{
	return totalHwyDistance;
}

double VehicleTrip::getTotalTimeinMinute()
{
	return totalTimeInMin;
}

void VehicleTrip::getTimeinStd(int *outStd)
{
	int roundTotalTimeInMin = round(totalTimeInMin);
	outStd[0] = totalTimeInMin / (24 * 60);
	outStd[1] = (roundTotalTimeInMin % (24 * 60)) / 60;
	outStd[2] = (roundTotalTimeInMin % (24 * 60)) % 60;
}

double VehicleTrip::getRestFuel()
{
	return restFuel;
}

double VehicleTrip::getFuelAdded()
{
	return fuelAdded;
}

double VehicleTrip::getTotalFuelUsed()
{
	return totalFuelUsed;
}

int VehicleTrip::getFuelAddCntr()
{
	return fuelAddCntr;
}
