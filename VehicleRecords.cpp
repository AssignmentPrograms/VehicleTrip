/*
 * File Name: VehicleRecords.cpp
 * Author: Madhav Gautam
 * Student ID: P428j884
 * Assignment Number: #2
 */

#include <iostream>
#include <map>
#include <cstdlib>
#include <string>
#include <sstream>
#include <fstream>
#include <algorithm>


#include "VehicleRecords.h"

//std::vector<Vehicle> Vehiclefields::vehicles;

VehicleRecords::VehicleRecords()
{
	// constructor is empty.
	// We don't initialize static data member here,
	// because static data initialization will happen on every constructor call.
}

VehicleRecords::VehicleRecords(char fileName[80])
{
	try {
		std::string line, key, o,d;
		ifstream instream(fileName);
		if(instream.fail()) {
			throw 'f';
		}
		numberOfVehicles = 0;

		/* check for good file and write to structure by looping through */

			while (std::getline(instream, line)) {

				/* skip any leading whitespace and line termination characters */
				std::string::size_type begin = line.find_first_not_of(" \f\t\v\n");

				/* blank lines */
				if (begin == std::string::npos) continue;

				/* Skip commentary */
				if (std::string("#").find(line[begin]) != std::string::npos) continue;

				std::string field;

				/* initializing seperate fields vector */
				std::vector<std::string> separated_fields;

				/* ing a stringstream to split the results based off | character */
				std::istringstream iss_line(line);

				int valueCounter = 1;
				/* Read in first vehicle information, take care of any overhead comments*/
				Vehicle* vehicle1 = new Vehicle;

				/*  Split line on the '|' character */
				while (std::getline(iss_line, field, '|')) {

					// Vector containing each field
					separated_fields.push_back(field);
					if(valueCounter !=1 && valueCounter != 2) {
						remove( field.begin(), field.end(), ' ' );
						if( field.find_first_not_of("1234567890.-\n\r") != string::npos ) {
							cout << "invalid number: " << field << endl;
							throw field;
						}
					}
					switch(valueCounter) {
					case 1:
							//manufacturer = field;
							vehicle1->setManufacturer(field);
							break;
					case 2:
							//model = field;
							vehicle1->setModel(field);
							break;
					case 3:
							//engine = CharToFloat(field, charCounter);
							vehicle1->setEngine(atof(field.c_str()));
							break;
					case 4:
							//cylinder = CharToFloat(field, charCounter);
							vehicle1->setCylinder(atof(field.c_str()));
							break;
					case 5:
							//tankSize = CharToFloat(field, charCounter);
							vehicle1->setTanksize(atof(field.c_str()));
							break;
					case 6:
							//cityMPG = CharToFloat(field, charCounter);
							vehicle1->setCityMPG(atof(field.c_str()));
							break;
					case 7:
							//hwyMPG = CharToFloat(field, charCounter);
							vehicle1->setHwyMPG(atof(field.c_str()));
							break;
					}
					valueCounter++;
				}
				if (vehicle1->getEngine() != 0) {
				  vehicles.push_back(vehicle1);
				  numberOfVehicles++;
				}
			}

		/* close file */
		instream.close();
	}
	catch (char e) {
		cerr << "\"" << fileName << "\"" << " file failed to open." << endl;
		cerr << "Please check if this file \"" << fileName << "\" is present in project location.\n" << endl;
		cerr << "Program will exit now!!!!";
		std::cin.get();
		exit(1);
	}

	/* exception catch if non digit included in input parameter value */
	catch (string e) {
		cerr << "Error while converting string \"" << e << "\" in int or double in \"" << fileName << "\" File." << endl;
		cerr << "please correct string \"" << e << "\" with proper digit so that it can convert \n" << endl;
		cerr << "Program will exit now!!!!";
		std::cin.get();
		exit(-1);
}
}
/* Print out Vehicle Records to console. This is for debuging purposes only*/
void VehicleRecords::PrintVehicleRecords()
{
	for(int i = 0; i < numberOfVehicles; i++) {
		cout << "Manufacturer = " << vehicles[i]->getManufacturer() << endl;
		cout << "Model = " << vehicles[i]->getModel() << endl;
		cout << "Engine = " << vehicles[i]->getEngine() << endl;
		cout << "Cylinder = " << vehicles[i]->getCylinder() << endl;
		cout << "TankSize = " << vehicles[i]->getTanksize() << endl;
		cout << "CityMPG = " << vehicles[i]->getCityMPG() << endl;
		cout << "HwyMPG = " << vehicles[i]->getHwyMPG() << endl;
		std::cin.get();
	}
}

/* Return the private variable "numberOfVehicles", representing how many vehicles are in member
 * vector "vehicles" */
int VehicleRecords::getNumberOfVehicles()
{
    return numberOfVehicles;
}

/* Return the member vector vehicles */
vector<Vehicle*> VehicleRecords::getVehicleVector()
{
    return vehicles;
}
