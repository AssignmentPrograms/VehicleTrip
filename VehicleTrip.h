/*
 * File Name: VehicleTrip.h
 * Author: Madhav Gautam
 * Student ID: P428j884
 * Assignment Number: #2
 */

#ifndef VEHICLETRIP_H_
#define VEHICLETRIP_H_

#include "Vehicle.h"
#include "TripLeg.h"
#include <iostream>
class VehicleTrip
{
public:
	/* Default constructor */
	VehicleTrip();

	~VehicleTrip();

	/* Constructor with parameters */
	VehicleTrip(Vehicle* singleVehicle,
				TripLeg tripLegArray[],
				int tripLegLength,
				int avgSpeed_City,
				int avgSpeed_Hwy,
				double avgFuelPrice,
				double distanceGasStation,
				double timeNapGap,
				int refuelTime,
				int restRoomTime,
				int napTime);

	/* Copy constructor
	 * NOTE: This will be called even by parameters that do not containing
	 *'&' resulting in a copy being created as opposed to passing in address
	 */
	VehicleTrip(const VehicleTrip &vehicleTrip);

	VehicleTrip& operator=(const VehicleTrip &vehicleTrip);

	/* Accessor functions */
	double getTotalCostFuelUsed();
	double getTotalCostFuelAdded();
	double getTotalCityDriven();
	double getTotalHwyDriven();
	double getTotalTimeinMinute();
	void getTimeinStd(int *outStd);
	double getRestFuel();
	double getFuelAdded();
	double getTotalFuelUsed();
	int getFuelAddCntr();

	/* Function to check and calculate amount of fuel added */
	double isFuelAdded(double currentDistance,
					   double nextDistance,
					   double lastRestFuel,
					   int cityMPG,
					   int mpg,
					   double tankSize,
					   double distanceGasStation);

private:
		Vehicle* vehicle;

		/* Vehicle trip data members */
		int fuelAddCntr;
		double totalCostFuelUsed;
		double totalCostFuelAdded;
		double totalCityDistance;
		double totalHwyDistance;
		double totalDriveTime;
		double totalTimeInMin;
		double restFuel;
		double fuelAdded;
		double totalFuelUsed;
};

#endif /* VEHICLETRIP_H_ */
